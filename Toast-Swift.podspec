Pod::Spec.new do |s|
  s.name         = "Toast-Swiftv4"
  s.version      = "4.0.0"
  s.summary      = "A Swift extension that adds toast notifications to the UIView object class."
  s.homepage     = "https://bitbucket.org/melnichukal2/toast-swift"
  s.license      = 'MIT'
  s.author       = { "Charles Scalesse" => "scalessec@gmail.com" }
  s.source       = { :git => "https://melnichukal2@bitbucket.org/melnichukal2/toast-swift.git" }
  s.platform     = :ios
  s.source_files = 'Toast'   
  s.framework    = 'QuartzCore'
  s.requires_arc = true
  s.ios.deployment_target = '8.0'
end
